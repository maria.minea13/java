package com.sda.oopex.task1;

public class Point3D extends Point2D {
    private float z;

    public Point3D(float x, float y, float z){
        super(x,y);
        this.z = z;
    }

    public float getZ(){
        return this.z;
    }

    public float[] getXYZ(){
        float[] xyz = {x, y, z};
        return xyz;
    }

    public void setZ(float z){
        this.z = z;

    }

    public void setXYZ(float x, float y, float z){
        super.setXY(x, y);
        this.z = z;
    }

    @Override
    public  String toString() {
        return String.format("(%.2f, %.2f, %.2f)", this.x, this.y, this.z);
    }
}

