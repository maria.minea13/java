package com.sda.shape;

import com.sda.shape.Circle;
import com.sda.shape.Rectangle;
import com.sda.shape.Shape;
import com.sda.shape.Triangle;

public class Main {

    public static void main(String[] args) {
        Shape[] shapes = new Shape[10];
        shapes[0] = new Triangle(2, 4);
        shapes[1] = new Circle(5);
        shapes[2] = new Rectangle(2, 4);
        shapes[3] = new Triangle(5, 4);
        shapes[4] = new Triangle(12, 4);
        shapes[5] = new Circle(6);
        shapes[6] = new Rectangle(8, 10);
        shapes[7] = new Circle(8);

        for (int i = 0; i <= 7; i++) {
            System.out.println(shapes[i].getName() + " with area of " + shapes[i].getArea());
        }
    }

    }
